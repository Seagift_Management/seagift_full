<?php
include("connect.php"); 
session_start();
//error_reporting(0);
if($_SESSION['hxt']==null)
    {
		header("Location:index.php");
	}
	
	echo("Welcome"." ".$_SESSION['hxt']);
?>

<!DOCTYPE html>
<html>
<!-- Mirrored from webdesign-finder.com/phototravel/contacts.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 10 May 2016 09:32:01 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Photo Travel">
    <meta name="keywords" content="Photo Travel">
    <title>Seagift</title>
    <!--pageMeta-->
    <!-- Loading Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Loading Elements Styles -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/rt_icons.css" rel="stylesheet">
    <link href="css/prettyPhoto.css"  rel="stylesheet"/>
    <!-- Favicons -->
    <link rel="icon" href="images/favicons/favicon.png">
    <link rel="apple-touch-icon" href="images/favicons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-touch-icon-114x114.png">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="scripts/html5shiv.js"></script>
    <script src="scripts/respond.min.js"></script>
    <![endif]-->
    <!--headerIncludes-->
    </head>
    <body>
    <div id="preloader">
        <div class="loading-data">
            <div class="dot"></div>
            <div class="dot2"></div>
        </div>
    </div>
     <div id="wrap">
        <nav class="navbar">
             <div class="container">
                 <a href="#" class="navbar-brand"><span>SEA</span><img height="29" alt="Your logo" src="images/logo.png">GIFT</a>
                 <button class="navbar-toggle menu-collapse-btn collapsed" data-toggle="collapse" data-target=".navMenuCollapse"  aria-hidden="true">
                     <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                 <div class="social_search-section">
                    
                     <ul class="soc-list">
                        <li><a href="lgout.php"  title="logout"><span class="icon soc_facebook">L</span></a></li>
                     </ul>
                 </div>
                 <div class="collapse navbar-collapse navMenuCollapse">
                    <ul class="nav">
<li ><a href="adminhome.php">HOME</a>                           
                        </li>
                        <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">STAFF</a>
                            <ul class="dropdown-menu">
                                <li><a href="staff_reg.php">Registration</a>
                                 <li><a href="adminvstaff.php">View</a>
                            </ul>
                        </li>
                        <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">CUSTOMER</a>
                            <ul class="dropdown-menu">
                                <li><a href="cust_reg.php">Registration</a>
                                <li><a href="adminvcust.php">View</a>
                                
                            </ul>
                        </li>
                        <li><a href="adminbill.php">BILL GENERATION</a></li>
                         <li><a href="vadminstock.php">STOCK</a></li>
                          <li><a href="vadminreturn.php">RETURNED ITEM</a></li>
                       
                           <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">VIEW BILLS</a>
                         <ul class="dropdown-menu">
                             <li><a href="admin_vbill.php">CUSTOMER</a>
                             <li><a href="pdf.php">CASH BALANCE</a>
                             <li><a href="adminincome_vbill.php">INCOME</a>
                             <li><a href="adminexpense_vbill.php">EXPENSE</a>
                            
                                
                            </ul></li>
                             <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">REPORT</a>
                         <ul class="dropdown-menu">
                             <li><a href="report_item.php">ITEM</a>
                             <li><a href="report_month.php">MONTH</a>
                             
                            
                                
                            </ul></li>
                        
                        
                    </ul>
                 </div>
             </div>
         </nav>
		<header id="breadcrumbs" class="dark">
            <div class="container">
                <div class="row">
                    <ul>
                        <li><a href="#" title="Homepage">HOME</a> </li>
                        <li><a href="#" title="Homepage">CUSTOMER</a> </li>
                        <li><a href="#" title="Homepage" class="active">REGISTRATION</a> </li>
                    </ul>
                </div>
            </div>
		</header>

        <main id="contacts" class="text-center">
           
            
            <div class="row common-block light">
                 <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                      
                      <h4>Customer Registration</h4>
                         <div class="form-container">
                              <form role="form"  name="cust_reg" onsubmit="return userValidation()" action="stfreg.php" method="post">
   
     <script >
	  function  chkUNAME()//--------Validation for User NAME field--------
{
   var user=cust_reg.uname.value;
    
	if(user =='')		//--------Validation for USERNAME field--------
			  		{   
					    document.getElementById("unme").innerHTML='<span style="color:#FF0000">Please Enter Your Username...!</span>';
						
				  		//alert("Please enter Your Username...!");
				  		cust_reg.uname.focus();
			  			return false;
			  		}	
	       var user =document.cust_reg.uname.value.search(/^[a-zA-Z]+(([0-9][a-zA-Z ])?[a-zA-Z]*)*$/); 
			  	if(user == -1)
			  		{
			  			document.getElementById("unme").innerHTML='<span style="color:#FF0000">Invalid Username...Pls enter valid one!</span>';
						
			  			document.cust_reg.uname.focus();
			  			return false;
			  		}     
	    	 	else
	              {
		           document.getElementById("unme").innerHTML="";
	              }
}

function  chkPWD()//--------Validation for Password field--------
{
   var pass=cust_reg.password.value;
   if(pass=='')		//--------Validation for PASSWORD field--------
			  		{
				  		 document.getElementById("pwd").innerHTML='<span style="color:#FF0000">Please enter your Password!</span>';
						//alert("Please enter Your Password!");
				  		cust_reg.password.focus();
			  			return false;
			  		}
				var pass=document.cust_reg.password.value.length;	 
				if(pass <= 7)
					{
						 document.getElementById("pwd").innerHTML='<span style="color:#FF0000">Password must contain 8 digits</span>';
						//alert('Password must contain 8 digits');
						document.cust_reg.password.focus();
						return false;
					}
                   else
	              {
		           document.getElementById("pwd").innerHTML="";
	              }
}
	     
    function chkNAME()//--------Validation for NAME field--------
{
	var name=cust_reg.name.value;
                  if(name=='') 		//--------Validation for NAME field--------
			  		{
			  			document.getElementById("fnme").innerHTML='<span style="color:#FF0000">Please Enter Your Name...!</span>';
						//alert("Please Enter Your Name...!");
			  			cust_reg.name.focus();
			  			return false;
			  		}
				var name =document.cust_reg.name.value.search(/^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/); 
			  	if(name == -1)
			  		{
			  			document.getElementById("fnme").innerHTML='<span style="color:#FF0000">Invalid Name....!</span>';
						//alert('Invalid Name....!');
			  			document.cust_reg.name.focus();
			  			return false;
			  		}
					
					else
	              {
		           document.getElementById("fnme").innerHTML="";
	              }
}
function chkADDRESS()//--------Validation for ADDRESS field--------
{
var add=cust_reg.address.value;
	if(add =='')		
			  		{
				  		document.getElementById("adrs").innerHTML='<span style="color:#FF0000">Please Enter Your Address!</span>';
						//alert("Please Enter Your Address!");
				  		cust_reg.address.focus();
			  			return false;
			  		}
			  	
			  	var add=document.cust_reg.address.value.length;	 
				if(add<=2 || add>=100)
					{
						document.getElementById("adrs").innerHTML='<span style="color:#FF0000">Address character limit between 3 and 100</span>';
						//alert('');
						document.cust_reg.address.focus();
						return false;
					}	
					var add =document.cust_reg.address.value.search(/^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/); 
			  	if(add == -1)
			  		{
			  			document.getElementById("adrs").innerHTML='<span style="color:#FF0000">Invalid Address....!</span>';
						//alert('Invalid Name....!');
			  			document.cust_reg.address.focus();
			  			return false;
			  		}
						else
	              {
		           document.getElementById("adrs").innerHTML="";
	              }
}	
function chkMAIL()
{
	var eml=cust_reg.email.value;
	
	
	  if(eml =='')		//--------Validation for EMAIL ID field--------
				  	{
					  	document.getElementById("emil").innerHTML='<span style="color:#FF0000">Please Enter Your Email Id!</span>';//alert("Please Enter Your Email Id!");
					  	cust_reg.email.focus();
				  		return false;
				  	}
				var eml =document.cust_reg.email.value.search(/^[a-zA-Z0-9_]([a-zA-Z0-9][_\.\-]?)*\@[a-zA-Z0-9_\-]+(\.[a-zA-Z]+){0,}\.[a-zA-Z]{2,6}$/); 
				if(eml == -1)
			        {
				        document.getElementById("emil").innerHTML='<span style="color:#FF0000">Invalid EmailId...!</span>';//alert('Invalid EmailId...!');
				        document.cust_reg.email.focus();
				        return false;
			        }

      else
	  {
		document.getElementById("emil").innerHTML="";
	  }	  
    
}
function chkPHONE1()
{
var phno=cust_reg.phone1.value;
if(phno =='')		//--------Validation for PHONE NUMBER field--------
			  		{
				  		document.getElementById("phone11").innerHTML='<span style="color:#FF0000">Please Enter Your Phone Number!</span>';//alert("Please Enter Your Phone Number!");
				  		cust_reg.phone1.focus();
			  			return false;
			  		}
				var phno =document.cust_reg.phone1.value.search(/^[0-9]+$/); 
				if(phno == -1)
				    {
				        document.getElementById("phone11").innerHTML='<span style="color:#FF0000">Invalid Phone Number!</span>';//alert('Invalid Phone Number!');
				        document.cust_reg.phone1.focus();
				        return false;
				    }
				
				var phno=document.cust_reg.phone1.value.length;	 
				if(phno<10 || phno>10 )
					{
						document.getElementById("phone11").innerHTML='<span style="color:#FF0000">Phone number must contain 10 digits</span>';//alert('Phone number must contain 10 digits');
						document.cust_reg.phone1.focus();
						return false;
					}
					else
	              {
		           document.getElementById("phone11").innerHTML="";
	              }
}
function  chkTID()//--------Validation for id card type--------
	{
		var tr=cust_reg.select.value;
		   
          	   if(tr =='--select--')		
		  		    {
			  		 document.getElementById("tid").innerHTML='<span style="color:#FF0000">Please select ID Card...!</span>';
			  		 cust_reg.select.focus();
		  			 return false;
		  		    }	
		       else
	              {
		           document.getElementById("tid").innerHTML="";
	              }
}
 function  chkID()//--------Validation for id number field--------
{
   var user=cust_reg.idno.value;
    
	if(user =='')		//--------Validation for USERNAME field--------
			  		{   
					    document.getElementById("idn").innerHTML='<span style="color:#FF0000">Please Enter Your Id CardNumber...!</span>';
				  		//alert("Please enter Your Username...!");
				  		cust_reg.idno.focus();
			  			return false;
			  		}	
	       var user =document.staff_reg.idno.value.search(/^[a-zA-Z0-9]+$/);
			  	if(user == -1)
			  		{
			  			document.getElementById("idn").innerHTML='<span style="color:#FF0000">Invalid Id CardNumber...Pls enter valid one!</span>';
						
			  			document.cust_reg.idno.focus();
			  			return false;
			  		}     
	    	 	else
	              {
		           document.getElementById("idn").innerHTML="";
	              }
}
function  chkPHOTO()
	{
		var photo=cust_reg.pic.value;
		if(photo =='Browse' || photo=='')		//--------Validation for IMAGE field--------
		  		{
			  		 document.getElementById("photo").innerHTML='<span style="color:#FF0000">Please Upload Your Photo....!</span>';//alert("Please Upload Your Photo....!");		
			  		cust_reg.pic.focus();
		  			return false;
		  		}
				else
	              {
		           document.getElementById("photo").innerHTML="";
	              }
}
   		

	
	</script>

   
   
   
   
    <div class="form-group">
    <label for="exampleInputEmail1">Username<span class="mand">*</span></label>
    <span id="sprytextfield1">
    <input type="text" class="form-control" name="uname" id="uname" placeholder="User Name" required onblur="chkUNAME()">
     <span class="mandedit" id="unme"></span>
     </div>
    <div class="form-group">
    <label for="exampleInputPassword1">Password<span class="mand">*</span></label>
    <input type="password" class="form-control" name="password" id="password" placeholder="Password" required onBlur="chkPWD()">
                       <span class="mandedit" id="pwd"></span>
  </div>
   
   <div class="form-group">
    <label for="exampleInputEmail1">Name<span class="mand">*</span></label>
    <span id="sprytextfield1">
    <input type="text" class="form-control" name="name" id="name" placeholder="First Name" required onBlur="chkNAME()"/>
     <span class="mandedit" id="fnme"></span>
     </div>
    
    	
     
    <div class="form-group">
    <label for="exampleInputEmail1">Address<span class="mand">*</span></label>
    <span id="sprytextfield1">
    <input type="text" class="form-control" name="address" id="address" placeholder="Address" required  onBlur="chkADDRESS()"/>
    <span class="mandedit" id="adrs"></span>
    </div>
  
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="text" class="form-control" name="email" id="email" placeholder="Email" onBlur="chkMAIL()" >
    <span class="mandedit" id="emil"></span> 
  </div>
  
 
  
      <div class="form-group">
    <label for="exampleInputEmail1">Phone_No.<span class="mand">*</span></label>
    <input type="text" class="form-control" name="phone1" id="phone1" placeholder="Phone_No 1" required  onBlur="chkPHONE1()" >
     <span class="mandedit" id="phone11"></span> 
  </div>
  
   <div class="mws-form-item">
     <div class="form-group">
      <label for="exampleInputEmail1">Type of Identity</label>
 
                                            <select class="form-control" name="tid" id="tid" placeholder="--select--" onBlur="chkTID()">
                                              
                                                                                     
                                                <option>Election id</option>
                                           		<option>Driving License</option>
                                            </select>
                                            <span class="mandedit" id="tid"></span> 
                                        </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Id card No.<span class="mand">*</span></label>
    <span id="sprytextfield1">
    <input type="text" class="form-control" name="idno" id="idno" placeholder="First Name" required  onblur="chkID()"><span class="mandedit" id="idn"></span>
     </div>
   
   <div class="form-group">
    <label for="exampleInputEmail1">Photo</label>
    <span id="sprytextfield1">
    <input type="file" name="pic"  onBlur="chkPHOTO()"><span class="mandedit" id="photo"></span>
     </div>
    
  
    
 
  <button type="submit" name="b3" class="btn btn-sm btn-warning"> REGISTER</button>
  
<script>
function myFunction() {
    alert("Are you sure!");
}
</script>
</form>
                         </div>
                 </div>
            </div>

        </main>
         <section id="instagram">
            
             <ul class="instagram-list gallery">
                 <li> <a href="images/insta_01.jpg" class="PhotoZoom" title="Description" ><img src="images/insta_01.jpg" alt=""></a></li>
                 <li> <a href="images/insta_02.jpg" class="PhotoZoom" title="Description"><img src="images/insta_02.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_03.jpg" class="PhotoZoom" title="Description"><img src="images/insta_03.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_04.jpg" class="PhotoZoom" title="Description"><img src="images/insta_04.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_05.jpg" class="PhotoZoom" title="Description"><img src="images/insta_05.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_06.jpg" class="PhotoZoom" title="Description"><img src="images/insta_06.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_07.jpg" class="PhotoZoom" title="Description"><img src="images/insta_07.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_08.jpg" class="PhotoZoom" title="Description"><img src="images/insta_08.jpg" alt=""></a> </li>
             </ul>
         </section>
        <footer class="footer-social-sec dark">
            <div class="container">
                <div class="row">
                    
                </div>
            </div>
        </footer>
        <footer class="underground dark">
            <div class="container">
                <div class="row">
                    <p>
                        &copy; Seagift 2016 | Created  by <span> Arun.s.babu and shafeer </span>
                    </p>
                </div>
            </div>
        </footer>
     </div>
    <!-- /#wrap -->
     <script src="scripts/jquery.validate.min.js"></script>
     <script src="scripts/placeholders.jquery.min.js"></script>
     <script>
         function runScript() {
             $('#contact_form').validate({
                 onfocusout: false,
                 onkeyup: false,
                 rules: {
                     name: "required",
                     message: "required",
                     phone: "required"
                 },
                 errorPlacement: function (error, element) {
                     error.insertAfter(element);
                 },
                 messages: {
                     name: "What's your name?",
                     message: "Type your message",
                     phone: "Enter your phone"
                 },

                 highlight: function (element) {
                     $(element)
                             .text('').addClass('error')
                 },

                 success: function (element) {
                     element
                             .text('').addClass('valid')
                 }
             });

             $('#contact_form').submit(function () {
                 // submit the form
                 if ($(this).valid()) {
                     $('#contact_submit').button('loading');
                     var action = $(this).attr('action');
                     $.ajax({
                         url: action,
                         type: 'POST',
                         data: {
                             contactname: $('#contact_name').val(),
                             contactphone: $('#number').val(),
                             contactmessage: $('#contact_message').val()
                         },
                         success: function () {
                             $('#contact_submit').button('reset');
                             //Use modal popups to display messages
                             $('#modalContactSuccess').modal('show');
                         },
                         error: function () {
                             $('#contact_submit').button('reset');
                             //Use modal popups to display messages
                             $('#modalContactError').modal('show');
                         }
                     });
                 } else {
                     $('#contact_submit').button('reset')
                 }
                 return false;
             });
         }

         runScript();

         document.addEventListener('reload-script',function(){
             runScript();
         });
     </script>
     <script src="scripts/jquery-1.12.3.min.js"></script>
     <script src="scripts/jquery.prettyPhoto.js"></script>
     <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
     <script src="scripts/preloader.js"></script>
     <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
     <script>
         var map;
         var egglabs = new google.maps.LatLng(51.9730757, 1.1502547);
         var mapCoordinates = new google.maps.LatLng(51.9730757, 1.1502547);
         var markers = [];
         var image = new google.maps.MarkerImage(
                 'images/marker.png',
                 new google.maps.Size(84,56),
                 new google.maps.Point(0,0),
                 new google.maps.Point(42,56)
         );

         function addMarker()
         {
             markers.push(new google.maps.Marker({
                 position: egglabs,
                 raiseOnDrag: false,
                 icon: image,
                 map: map,
                 draggable: false
             }));

         }

         function initialize() {
             var mapOptions = {
                 backgroundColor: "#f2e5d4",
                 zoom: 14,
                 disableDefaultUI: true,
                 center: mapCoordinates,
                 mapTypeId: google.maps.MapTypeId.ROADMAP,
                 styles: [
                     {
                         "featureType": "landscape.natural",
                         "elementType": "geometry.fill",
                         "stylers": [
                             { "color": "#f2e5d4" }
                         ]
                     },
                     {
                         "featureType": "landscape.man_made",
                         "stylers": [
                             { "color": "#f2e5d4" },
                             { "visibility": "off" }
                         ]
                     },
                     {
                         "featureType": "water",
                         "stylers": [
                             { "color": "#80C8E5" },
                             { "saturation": 0 }
                         ]
                     },
                     {
                         "featureType": "road.arterial",
                         "elementType": "geometry",
                         "stylers": [
                             { "color": "#e5d8c8" }
                         ]
                     }
                     ,{
                         "elementType": "labels.text.stroke",
                         "stylers": [
                             { "visibility": "off" }
                         ]
                     }
                     ,{
                         "elementType": "labels.text",
                         "stylers": [
                             { "color": "#3e3933" }
                         ]
                     }

                     ,{
                         "featureType": "road.local",
                         "stylers": [
                             { "color": "#dedede" }
                         ]
                     }
                     ,{
                         "featureType": "road.local",
                         "elementType": "labels.text",
                         "stylers": [
                             { "color": "#989898" }
                         ]
                     }
                     ,{
                         "featureType": "transit.station.bus",
                         "stylers": [
                             { "saturation": -57 }
                         ]
                     }
                     ,{
                         "featureType": "road.highway",
                         "elementType": "labels.icon",
                         "stylers": [
                             { "visibility": "off" }
                         ]
                     },{
                         "featureType": "poi",
                         "stylers": [
                             { "visibility": "off" }
                         ]
                     }

                 ]

             };
             map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
             addMarker();

         }
         google.maps.event.addDomListener(window, 'load', initialize);

     </script>
</body>
<!-- Mirrored from webdesign-finder.com/phototravel/contacts.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 10 May 2016 09:32:02 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
</html>