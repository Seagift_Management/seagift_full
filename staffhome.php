<?php
include("connect.php"); 
session_start();
//error_reporting(0);
if($_SESSION['din']==null)
    {
		header("Location:index.php");
	}
	
	//echo("Welcome"." ".$_SESSION['din']);
?>

<!--shows staff work area-->
<!DOCTYPE html>
<html>
<!-- Mirrored from webdesign-finder.com/phototravel/homepage_2.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 10 May 2016 09:31:35 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Photo Travel">
    <meta name="keywords" content="Photo Travel">
    <title>Seagift</title>
    <!--pageMeta-->
    <!-- Loading Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Loading Elements Styles -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/rt_icons.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet" >
    <link href="css/prettyPhoto.css"  rel="stylesheet"/>
    <link href="css/owl.carousel.css"  rel="stylesheet"/>
    <link href="css/flexslider.css"  rel="stylesheet"/>
    <!-- Favicons -->
    <link rel="icon" href="images/favicons/favicon.png">
    <link rel="apple-touch-icon" href="images/favicons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-touch-icon-114x114.png">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="scripts/html5shiv.js"></script>
    <script src="scripts/respond.min.js"></script>
    <![endif]-->
    <!--headerIncludes-->
    </head>
    <body id="index2">
    <div id="preloader">
        <div class="loading-data">
            <div class="dot"></div>
            <div class="dot-b"></div>
        </div>
    </div>
    <div id="wrap">
        <nav class="navbar">
            <div class="container">
                <a href="#" class="navbar-brand"><span>SEA</span><img height="29" alt="Your logo" src="images/logo.png">GIFT</a>
                <button class="navbar-toggle menu-collapse-btn collapsed" data-toggle="collapse" data-target=".navMenuCollapse"  aria-hidden="true">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <div class="social_search-section">
                    
                    <ul class="soc-list">
                        <li><a href="lgout.php"  title="logout"><span class="icon soc_facebook">L</span></a></li>
                       
                    </ul>
                </div>
                <div class="collapse navbar-collapse navMenuCollapse menu-mode-3">
                    <ul class="nav">
                       <li><a href="staffhome.php">HOME</a></li>
                           
                        </li>
                        
                        <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">VIEW</a>
                            <ul class="dropdown-menu">
                             <li><a href="vcust.php">CUSTOMER</a>
                             <li><a href="vexp.php">EXPENSE</a>
                             <li><a href="vincome.php">INCOME</a>
							 <li><a href="vpur.php">PURCHASE</a>
							 <li><a href="vcat.php">CATEGORY</a>
                             <li><a href="vstock.php">STOCK</a> 
                                
                            </ul>
                        </li>
                         <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">ENTRY</a>
                         <ul class="dropdown-menu">
                             <li><a href="pur_entry.php">PURCHASE ENTRY</a>
                             <li><a href="trans.php">TRANSACTION ENTRY</a>
                             <li><a href="income.php">INCOME ENTRY</a>
                             <li><a href="expense.php">EXPENSE ENTRY</a>
                             <li><a href="stock.php">STOCK ENTRY</a>
                             <li><a href="return.php">RETURNED GOODS</a>
                             
                                
                            </ul></li>
                          <li><a href="staffbill.php">BILLING</a></li>
                           <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">VIEW BILLS</a>
                         <ul class="dropdown-menu">
                             <li><a href="staff_vbill.php">CUSTOMER</a>
                             <li><a href="pdf.php">CASH BALANCE</a>
                             <li><a href="income_vbill.php">INCOME</a>
                             <li><a href="expense_vbill.php">EXPENSE</a>
                             <li><a href="purchase_vbill.php">PURCHASE</a>
                             <li><a href="stock_vbill.php">STOCK</a> 
                                
                            </ul></li>
                         
                    </ul>
                </div>
				<br>
            </div>
        </nav>
        <header class="main_slider s-mode light">
            <div class="container">
                <div id="slider" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="images/latest_photo_11.jpg" alt="" />
                           <!-- <a class="info-slider"></a>  -->
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>75</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>31</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                November 22, 2014 <br>
                                                October 19, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.588408110381!2d-0.15834819967910493!3d51.52076673608002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ace9a2e67d7%3A0xd458de8d0fdc498e!2zQmFrZXIgU3QsIE1hcnlsZWJvbmUsIExvbmRvbiwg0JLQtdC70LjQutC-0LHRgNC40YLQsNC90LjRjw!5e0!3m2!1sru!2sua!4v1457952134423"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_22.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>83</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>72</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                December 22, 2012 <br>
                                                November 11, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6402236.224204002!2d-121.51361300184796!3d38.41624087926714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80990aa1f8deb471%3A0xcf47038aaafc95b3!2z0J3QtdCy0LDQtNCwLCDQodCo0JA!5e0!3m2!1sru!2sua!4v1459385906165"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_55.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>32</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>24</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                November 05, 2013 <br>
                                                October 19, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.588408110381!2d-0.15834819967910493!3d51.52076673608002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ace9a2e67d7%3A0xd458de8d0fdc498e!2zQmFrZXIgU3QsIE1hcnlsZWJvbmUsIExvbmRvbiwg0JLQtdC70LjQutC-0LHRgNC40YLQsNC90LjRjw!5e0!3m2!1sru!2sua!4v1457952134423"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_33.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>83</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>72</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                December 22, 2012 <br>
                                                November 11, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6402236.224204002!2d-121.51361300184796!3d38.41624087926714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80990aa1f8deb471%3A0xcf47038aaafc95b3!2z0J3QtdCy0LDQtNCwLCDQodCo0JA!5e0!3m2!1sru!2sua!4v1459385906165"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_44.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>75</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>31</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                November 22, 2014 <br>
                                                October 19, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.588408110381!2d-0.15834819967910493!3d51.52076673608002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ace9a2e67d7%3A0xd458de8d0fdc498e!2zQmFrZXIgU3QsIE1hcnlsZWJvbmUsIExvbmRvbiwg0JLQtdC70LjQutC-0LHRgNC40YLQsNC90LjRjw!5e0!3m2!1sru!2sua!4v1457952134423"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_67.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>83</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>72</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                December 22, 2012 <br>
                                                November 11, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6402236.224204002!2d-121.51361300184796!3d38.41624087926714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80990aa1f8deb471%3A0xcf47038aaafc95b3!2z0J3QtdCy0LDQtNCwLCDQodCo0JA!5e0!3m2!1sru!2sua!4v1459385906165"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_55.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>75</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>31</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                November 22, 2014 <br>
                                                October 19, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.588408110381!2d-0.15834819967910493!3d51.52076673608002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ace9a2e67d7%3A0xd458de8d0fdc498e!2zQmFrZXIgU3QsIE1hcnlsZWJvbmUsIExvbmRvbiwg0JLQtdC70LjQutC-0LHRgNC40YLQsNC90LjRjw!5e0!3m2!1sru!2sua!4v1457952134423"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_44.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>83</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>72</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                December 22, 2012 <br>
                                                November 11, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6402236.224204002!2d-121.51361300184796!3d38.41624087926714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80990aa1f8deb471%3A0xcf47038aaafc95b3!2z0J3QtdCy0LDQtNCwLCDQodCo0JA!5e0!3m2!1sru!2sua!4v1459385906165"></iframe>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="author_carousel_actions">
                    
                    
                    <div class="carousel-strap">
                        <div id="carousel" class="flexslider">
                            <ul class="slides">
                                <li>
                                    <img src="images/thumb1.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb2.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb3.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb4.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb5.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb6.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb3.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb5.jpg" alt="" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </header>



	<!--	<section class="common-block light top_photos">
			
		</section>
    -->    
        <section class="common-block light">
            <div class="container">
              
                <div class="row">
                    <div class="owl-carousel blog-posts-slide">
                        <div class="item">
                           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                               <div class="blog-post">
                                   <div class="post-media">
                                       <a href="#"><img src="images/post_img1.jpg" class="img-responsive" alt="Misty Bridge Going Nowhere" data-time="December 29, 2015" data-description="Consetetur."></a>
                                   </div>
                                   
                               </div>
                           </div>
                           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                               <div class="blog-post">
                                   <div class="post-media">
                                       <div class="owl-carousel2">
                                           <div class="item"><a href="#"><img src="images/post_img2.jpg" alt="Misty Bridge Going Nowhere"></a></div>
                                           <div class="item"><a href="#"><img src="images/post_img3.jpg" alt="Misty Bridge Going Nowhere"></a></div>
                                           <div class="item"><a href="#"><img src="images/post_img4.jpg" alt="Misty Bridge Going Nowhere"></a></div>
                                       </div>

                                   </div>
                                   
                               </div>
                           </div>
                           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                               <div class="blog-post">
                                   <div class="post-media">
                                       <a href="#"><img src="images/post_img3.jpg" class="img-responsive" alt="Misty Bridge Going Nowhere" data-time="December 29, 2015" data-description="Consetetur."></a>
                                   </div>
                                  
                               </div>
                           </div>
                           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                               <div class="blog-post">
                                   <div class="post-media">
                                       <a href="blog_post_video.html" target="_blank"><div class="play"></div>
                                           <img src="images/post_img4.jpg" class="img-responsive" alt="Misty Bridge Going Nowhere" data-time="December 29, 2015" data-description="Consetetur.">
                                       </a>
                                   </div>
                                  
                               </div>
                           </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                <div class="blog-post">
                                    <div class="post-media">
                                        <a href="#"><img src="images/post_img1.jpg" class="img-responsive" alt="Misty Bridge Going Nowhere" data-time="December 29, 2015" data-description="Consetetur."></a>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                <div class="blog-post">
                                    <div class="post-media">
                                        <div class="owl-carousel2">
                                            <div class="item"><a href="#"><img src="images/post_img3.jpg" alt="Misty Bridge Going Nowhere"></a></div>
                                            <div class="item"><a href="#"><img src="images/post_img2.jpg" alt="Misty Bridge Going Nowhere"></a></div>
                                            <div class="item"><a href="#"><img src="images/post_img4.jpg" alt="Misty Bridge Going Nowhere"></a></div>
                                        </div>

                                    </div>
                                   
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                <div class="blog-post">
                                    <div class="post-media">
                                            <a href="#"><img src="images/post_img4.jpg" alt="Misty Bridge Going Nowhere"></a>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                <div class="blog-post">
                                    <div class="post-media">
                                        <a href="blog_post_video.html" target="_blank"><div class="play"></div>
                                            <img src="images/post_img2.jpg" class="img-responsive" alt="Misty Bridge Going Nowhere" data-time="December 29, 2015" data-description="Consetetur.">
                                        </a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clear_h"></div>
            </div>
        </section>
        <div class="cross-social">
             <!--     <ul class="soc-list">
                    <li><a href="#" target="_blank" title="facebook"><span class="icon soc_facebook">f</span></a></li>
                    <li><a href="#" target="_blank" title="twitter"><span class="icon soc_twitter">l</span></a></li>
                    <li><a href="#" target="_blank" title="google plus"><span class="icon soc_google">g</span></a></li>
                    <li><a href="#" target="_blank" title="linkedin"><span class="icon soc_linkedin">i</span></a></li>
                    <li><a href="#" target="_blank" title="flickr"><span class="icon soc_pinterest">:</span></a></li>
                    <li><a href="#" target="_blank" title="pinterest"><span class="icon soc_flickr">n</span></a></li>
                    <li><a href="#" target="_blank" title="rss"><span class="icon soc_rss">r</span></a></li>
                </ul>
			-->	
        </div>
        <footer class="footer dark">
            <div class="container">
                <div class="row-wider">
                    <div class="col-sm-6 col-md-3 col-lg-3 col-l-foot">
                        <a href="#" class="navbar-brand"><span>SEA</span><img height="29" alt="Your logo" src="images/foot_thumbnail.jpg">GIFT</a>
                        <p class="text-left">
(Automated Fish Vendor System)
<br>Market Road
<br>Ernakulam P.O.
<br>Kerala</p>
                       <!--     <ul class="pay-cards">
                                <li><a href="#" target="_blank" title="visa"><img src="images/foot_visa.jpg" alt="visa"></a></li>
                                <li><a href="#" target="_blank" title="discover"><img src="images/foot_discover.jpg" alt="visa"></a></li>
                                <li><a href="#" target="_blank" title="master card"><img src="images/foot_master.jpg" alt="visa"></a></li>
                                <li><a href="#" target="_blank" title="card"><img src="images/foot_card.jpg" alt="visa"></a></li>
                                <li><a href="#" target="_blank" title="paypal"><img src="images/foot_paypal.jpg" alt="visa"></a></li>
                            </ul>
						-->	
                    </div>
                    <div class="col-sm-5 col-md-2 col-lg-2 text-center col-nav-foot">
                        <h2>Links</h2> 
                        <ul>
                            <li><a href="staffhome.php">Homepages</a></li>
                            <li><a href="about.html">About us</a></li>
                            
                        </ul>
                    </div>
                   
                    <div class="col-sm-5 col-md-2 col-lg-3 footer_ads">
                    </div>
                </div>
            </div>
        </footer>
        <footer class="underground">
            <div class="container">
                <div class="row">
                    <p>
                        &copy; Seagift 2016 | Created  by <span> Arun.s.babu & shafeer </span>
                    </p>
                </div>
            </div>
        </footer>
    </div>
    <!-- /#wrap -->
    <!-- JavaScript -->
            <script src="scripts/jquery-1.12.3.min.js"></script>
            <script src="scripts/modernizr.custom.97074.js"></script>
            <script src="scripts/owl.carousel.js"></script>
            <script src="scripts/jquery.prettyPhoto.js"></script>
            <script type="text/javascript" src="scripts/jquery.hoverdir.js"></script>
            <script src="scripts/jPages.js"></script>
            <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
            <script type="text/javascript" src="scripts/placeholders.jquery.min.js"></script>
            <script type="text/javascript" src="scripts/jquery.flexslider-min.js"></script>
            <script src="scripts/custom.js"></script>
            <script type="text/javascript">
                $('.owl-carousel').owlCarousel({
                    items:1,
                    loop:true,
                    margin:0,
                    nav:true,
                    responsiveClass:true,
                    dots:false,
                    video:false,
                    videoWidth: false,
                    videoHeight: false,
                    merge:true,
                    lazyLoad:true,
                    center:true,
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:1
                        },
                        1000:{
                            items:1
                        }
                    }
                })
            </script>
            <script type="text/javascript">
                $('.owl-carousel2').owlCarousel({
                    items:1,
                    loop:true,
                    margin:0,
                    nav:true,
                    responsiveClass:true,
                    dots:true,
                    video:true,
                    videoWidth: false,
                    videoHeight: false,
                    merge:true,
                    lazyLoad:true,
                    center:true,
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:1
                        },
                        1000:{
                            items:1
                        }
                    }
                })
            </script>
</body>
<!-- Mirrored from webdesign-finder.com/phototravel/homepage_2.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 10 May 2016 09:31:45 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
</html>