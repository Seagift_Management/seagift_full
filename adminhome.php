<?php
include("connect.php"); 
session_start();
//error_reporting(0);
if($_SESSION['hxt']==null)
    {
		header("Location:index.php");
	}
	
	echo("Welcome"." ".$_SESSION['hxt']);
?>

<!DOCTYPE html>
<html>
<!-- Mirrored from webdesign-finder.com/phototravel/homepage_2.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 10 May 2016 09:31:35 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Photo Travel">
    <meta name="keywords" content="Photo Travel">
    <title>Seagift</title>
    <!--pageMeta-->
    <!-- Loading Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Loading Elements Styles -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/rt_icons.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet" >
    <link href="css/prettyPhoto.css"  rel="stylesheet"/>
    <link href="css/owl.carousel.css"  rel="stylesheet"/>
    <link href="css/flexslider.css"  rel="stylesheet"/>
    <!-- Favicons -->
    <link rel="icon" href="images/favicons/favicon.png">
    <link rel="apple-touch-icon" href="images/favicons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-touch-icon-114x114.png">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="scripts/html5shiv.js"></script>
    <script src="scripts/respond.min.js"></script>
    <![endif]-->
    <!--headerIncludes-->
    </head>
    <body id="index2">
    <div id="preloader">
        <div class="loading-data">
            <div class="dot"></div>
            <div class="dot-b"></div>
        </div>
    </div>
    <div id="wrap">
        <nav class="navbar">
            <div class="container">
                <a href="#" class="navbar-brand"><span>SEA</span><img height="29" alt="Your logo" src="images/logo.png">GIFT</a>
                <button class="navbar-toggle menu-collapse-btn collapsed" data-toggle="collapse" data-target=".navMenuCollapse"  aria-hidden="true">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <div class="social_search-section">
                    <ul class="soc-list">
                        <li><a href="lgout.php"  title="logout"><span class="icon soc_facebook">L</span></a></li>
                       
                    </ul> 
                   
                </div>
                <div class="collapse navbar-collapse navMenuCollapse menu-mode-3">
                    <ul class="nav">
                        <li><a href="adminhome.php">HOME</a>
                           
                        </li>
                        <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">STAFF</a>
                            <ul class="dropdown-menu">
                                <li><a href="staff_reg.php">Registration</a>
                                 <li><a href="adminvstaff.php">View</a>
                            </ul>
                        </li>
                        <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">CUSTOMER</a>
                            <ul class="dropdown-menu">
                                <li><a href="cust_reg.php">Registration</a>
                                <li><a href="adminvcust.php">View</a>
                                
                            </ul>
                        </li>
                        <li><a href="adminbill.php">BILL GENERATION</a></li>
                         <li><a href="vadminstock.php">STOCK</a></li>
                          <li><a href="vadminreturn.php">RETURNED ITEM</a></li>
                       
                           <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">VIEW BILLS</a>
                         <ul class="dropdown-menu">
                             <li><a href="admin_vbill.php">CUSTOMER</a>
                             <li><a href="pdf.php">CASH BALANCE</a>
                             <li><a href="adminincome_vbill.php">INCOME</a>
                             <li><a href="adminexpense_vbill.php">EXPENSE</a>
                            
                                
                            </ul></li>
                             <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">REPORT</a>
                         <ul class="dropdown-menu">
                             <li><a href="report_item.php">ITEM</a>
                             <li><a href="report_month.php">MONTH</a>
                             
                            
                                
                            </ul></li>
                        
                    </ul>
                </div>
            </div>
        </nav>
        <header class="main_slider s-mode light">
            <div class="container">
                <div id="slider" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="images/latest_photo_11.jpg" alt="" />
                           </li>
                        <li>
                            <img src="images/latest_photo_22.jpg" alt="" />
                           
                        </li>
                        <li>
                            <img src="images/latest_photo_55.jpg" alt="" />
                            
                        </li>
                        <li>
                            <img src="images/latest_photo_33.jpg" alt="" />
                            
                        </li>
                        <li>
                            <img src="images/latest_photo_44.jpg" alt="" />
                            
                        </li>
                        <li>
                            <img src="images/latest_photo_67.jpg" alt="" />
                           
                        </li>
                        <li>
                            <img src="images/latest_photo_55.jpg" alt="" />
                           
                        </li>
                        <li>
                            <img src="images/latest_photo_44.jpg" alt="" />
                           
                        </li>
                    </ul>
                </div>
                <div class="author_carousel_actions">
                    <div class="slider-author">
                        
                    </div>
                    
                    
                </div>
            </div>
            <div class="clear"></div>
        </header>



		<section class="common-block light top_photos">
			
		</section>
        <section class="common-block latest_photos">
            
        </section>
        
        <div class="cross-social">
                
        </div>
        <footer class="footer dark">
            <div class="container">
                <div class="row-wider">
                    <div class="col-sm-6 col-md-3 col-lg-3 col-l-foot">
                        <a href="#" class="navbar-brand"><span>SEA</span><img height="29" alt="Your logo" src="images/foot_thumbnail.jpg">GIFT</a>
                        <q class="text-left">
(Automated Fish Vendor System)
<br>Market Road
<br>Ernakulam P.O.
<br>Kerala</q>
                          
                    </div>
                    <div class="col-sm-5 col-md-2 col-lg-2 text-center col-nav-foot">
                       
                    </div>
                    
                    <div class="col-sm-5 col-md-2 col-lg-3 footer_ads">
                       
                    </div>
                </div>
            </div>
        </footer>
        <footer class="underground">
            <div class="container">
                <div class="row">
                    <p>
                        &copy; Seagift 2016 | Created  by <span> Arun.s.babu & shafeer </span>
                    </p>
                </div>
            </div>
        </footer>
    </div>
    <!-- /#wrap -->
    <!-- JavaScript -->
            <script src="scripts/jquery-1.12.3.min.js"></script>
            <script src="scripts/modernizr.custom.97074.js"></script>
            <script src="scripts/owl.carousel.js"></script>
            <script src="scripts/jquery.prettyPhoto.js"></script>
            <script type="text/javascript" src="scripts/jquery.hoverdir.js"></script>
            <script src="scripts/jPages.js"></script>
            <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
            <script type="text/javascript" src="scripts/placeholders.jquery.min.js"></script>
            <script type="text/javascript" src="scripts/jquery.flexslider-min.js"></script>
            <script src="scripts/custom.js"></script>
            <script type="text/javascript">
                $('.owl-carousel').owlCarousel({
                    items:1,
                    loop:true,
                    margin:0,
                    nav:true,
                    responsiveClass:true,
                    dots:false,
                    video:false,
                    videoWidth: false,
                    videoHeight: false,
                    merge:true,
                    lazyLoad:true,
                    center:true,
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:1
                        },
                        1000:{
                            items:1
                        }
                    }
                })
            </script>
            <script type="text/javascript">
                $('.owl-carousel2').owlCarousel({
                    items:1,
                    loop:true,
                    margin:0,
                    nav:true,
                    responsiveClass:true,
                    dots:true,
                    video:true,
                    videoWidth: false,
                    videoHeight: false,
                    merge:true,
                    lazyLoad:true,
                    center:true,
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:1
                        },
                        1000:{
                            items:1
                        }
                    }
                })
            </script>
</body>
<!-- Mirrored from webdesign-finder.com/phototravel/homepage_2.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 10 May 2016 09:31:45 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
</html>