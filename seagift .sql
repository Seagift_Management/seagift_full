-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 16, 2016 at 12:36 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `seagift`
--

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE IF NOT EXISTS `bill` (
  `bill_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NOT NULL,
  `bill_date` varchar(15) NOT NULL,
  `cur_bal_date` varchar(15) NOT NULL,
  `cur_bal_amt` int(11) NOT NULL,
  `cur_pur_date` varchar(15) NOT NULL,
  `pur_amt` int(11) NOT NULL,
  `income_id` int(11) DEFAULT NULL,
  `total_amt` int(11) NOT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`bill_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `bill`
--

INSERT INTO `bill` (`bill_id`, `cust_id`, `bill_date`, `cur_bal_date`, `cur_bal_amt`, `cur_pur_date`, `pur_amt`, `income_id`, `total_amt`, `remark`, `status`) VALUES
(6, 5, '07-AUG-2016', '07-AUG-2016', 193210, '07-AUG-2016', 10000, 8, 203210, NULL, 1),
(9, 0, '31-AUG-2016', '', 0, '31-AUG-2016', 0, 0, 0, NULL, 1),
(10, 3, '01-SEP-2016', '01-SEP-2016', 7612, '01-SEP-2016', 3513, 19, 11125, NULL, 1),
(2, 6, '03-AUG-2016', '07-AUG-2016', 77000, '03-AUG-2016', 22070, 8, 99070, NULL, 1),
(7, 3, '31-AUG-2016', '30-AUG-2016', 273110, '31-AUG-2016', 1703, 12, 274813, NULL, 1),
(8, 4, '31-AUG-2016', '31-AUG-2016	', 4000, '31-AUG-2016', 1775, 15, 5775, NULL, 1),
(11, 4, '01-SEP-2016', '01-SEP-2016', 4575, '01-SEP-2016', 5400, 17, 9975, NULL, 1),
(12, 4, '29-SEP-2016', '01-SEP-2016', 9975, '29-SEP-2016', 8502, 17, 18477, NULL, 1),
(13, 5, '13-OCT-2016', '13-OCT-2016', 801, '13-OCT-2016', 801, 20, 801, NULL, 1),
(14, 6, '31-AUG-2016', '31-AUG-2016	', 10001, '31-AUG-2016', 4631, 13, 10001, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cash_balance`
--

CREATE TABLE IF NOT EXISTS `cash_balance` (
  `balance_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NOT NULL,
  `bal_date` varchar(15) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`balance_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cash_balance`
--

INSERT INTO `cash_balance` (`balance_id`, `cust_id`, `bal_date`, `amount`) VALUES
(1, 5, '13-OCT-2016', 301),
(2, 3, '01-SEP-2016', 14638),
(3, 6, '31-AUG-2016', 10001),
(4, 4, '01-SEP-2016', 23877);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`name`) VALUES
('ayala'),
('chain'),
('choora'),
('kera'),
('meen'),
('ney'),
('neymean'),
('thilopiya');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `cust_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `image` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `phone` varchar(12) NOT NULL,
  `id_type` varchar(15) DEFAULT NULL,
  `idcardno` varchar(30) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`cust_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cust_id`, `name`, `address`, `image`, `email`, `phone`, `id_type`, `idcardno`, `remark`) VALUES
(3, 'sam', 'ekm', '6615sha.jpg', 'sam@gmail.com', '8967656454', 'Election id', 'k876', 'NULL'),
(4, 'arun', 'address', 'author.jpg', 'arun@gmail.com', '8988675645', 'Election id', 'did76786', 'NULL'),
(5, 'asura s', 'arunalayam,wadsa', 'latest_photo_1.jpg', 'asura@gmail.com', '8089801655', 'Election id', 'G4322', 'NULL'),
(6, 'sunil', 'ernakulam market', 'latest_photo_1.jpg', 'suni@gmail.com', '8864727474', 'Election id', 'Ak765674', 'NULL'),
(1, 'RETAIL', 'kochi', 'latest_photo_1.jpg', 'retail@gmail.com', '0000000000', 'No id', '0000000', 'cash purchasers');

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE IF NOT EXISTS `expense` (
  `expense_id` int(11) NOT NULL AUTO_INCREMENT,
  `expense_name` varchar(20) NOT NULL,
  `expense_date` varchar(15) NOT NULL,
  `expense_amt` int(11) NOT NULL,
  PRIMARY KEY (`expense_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`expense_id`, `expense_name`, `expense_date`, `expense_amt`) VALUES
(1, 'kit', '05-AUG-2016', 200),
(18, 'ere', '31-AUG-2016', 123),
(17, 'siyad', '07-AUG-2016', 500),
(16, 'kit', '07-AUG-2016', 100),
(15, 'ice', '07-AUG-2016', 100),
(14, 'loading', '06-AUG-2016', 40),
(13, 'auto', '05-AUG-2016', 200),
(12, 'ice', '05-AUG-2016', 120),
(9, 'ice', '06-AUG-2016', 170),
(10, 'nazeer', '06-AUG-2016', 600),
(11, 'diesel', '06-AUG-2016', 300),
(19, 'ice', '31-AUG-2016', 300),
(20, 'diesel', '31-AUG-2016', 300),
(21, 'siyad', '01-SEP-2016', 600),
(22, 'nazeer', '01-SEP-2016', 700),
(23, 'diesel', '01-SEP-2016', 300),
(24, 'loading', '01-SEP-2016', 250);

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE IF NOT EXISTS `income` (
  `income_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NOT NULL,
  `income_date` varchar(15) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`income_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `income`
--

INSERT INTO `income` (`income_id`, `cust_id`, `income_date`, `amount`) VALUES
(1, 3, '03-AUG-2016', 2000),
(5, 3, '04-AUG-2016', 10000),
(10, 6, '04-AUG-2016', 1234),
(9, 4, '04-AUG-2016', 2340),
(2, 5, '04-AUG-2016', 200),
(6, 3, '05-AUG-2016', 5000),
(7, 3, '06-AUG-2016', 2000),
(8, 3, '08-AUG-2016', 100000),
(11, 5, '30-AUG-2016', 120),
(12, 3, '30-AUG-2016', 100),
(13, 6, '31-AUG-2016', 1500),
(14, 3, '31-AUG-2016', 1200),
(15, 4, '31-AUG-2016', 300),
(16, 3, '01-SEP-2016', 1000),
(17, 4, '01-SEP-2016', 1200),
(18, 5, '01-SEP-2016', 700),
(19, 3, '01-SEP-2016', 5000),
(20, 5, '13-OCT-2016', 5000),
(21, 5, '13-OCT-2016', 500);

-- --------------------------------------------------------

--
-- Table structure for table `itemreturn`
--

CREATE TABLE IF NOT EXISTS `itemreturn` (
  `return_id` int(11) NOT NULL,
  `cust_id` int(11) NOT NULL,
  `item` varchar(15) NOT NULL,
  `return_date` varchar(15) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  `price` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `remarks` varchar(50) NOT NULL,
  PRIMARY KEY (`return_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itemreturn`
--

INSERT INTO `itemreturn` (`return_id`, `cust_id`, `item`, `return_date`, `weight`, `price`, `total`, `remarks`) VALUES
(23, 4, 'chain', '01-SEP-2016', '2.40', 120, 288, 'damaged'),
(22, 6, 'ayala', '01-SEP-2016', '1.50', 110, 165, 'damaged'),
(21, 5, 'meen', '31-AUG-2016', '67.89', 675, 45826, 'jhgfdsgjkl'),
(20, 3, 'ayala', '30-AUG-2016', '0.90', 123, 111, 'damage'),
(19, 3, 'choora', '08-AUG-2016', '4.00', 115, 460, 'fjtdfs'),
(18, 4, 'ayala', '09-AUG-2016', '3.00', 110, 330, 'fdyds'),
(16, 6, 'kera', '09-AUG-2016', '20.00', 115, 2300, 'damage'),
(17, 6, 'kera', '09-AUG-2016', '2.00', 100, 200, 'sfdsghjd'),
(14, 3, 'ayala', '07-AUG-2016', '5.00', 100, 500, 'damage'),
(15, 5, 'choora', '08-AUG-2016', '10.00', 120, 1200, 'not req');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(20) NOT NULL,
  `usertype` varchar(50) NOT NULL,
  `cust_id` int(11) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`, `usertype`, `cust_id`) VALUES
('admin', '12345678', 'admin', 0),
('staff', '12345678', 'staff', 0),
('asura', '12345678', 'customer', 5),
('suni', '123', 'customer', 6),
('arun', '123', 'customer', 4);

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE IF NOT EXISTS `purchase` (
  `pur_id` int(11) NOT NULL,
  `pur_name` varchar(20) NOT NULL,
  `pur_date` varchar(15) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`pur_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`pur_id`, `pur_name`, `pur_date`, `weight`, `price`) VALUES
(18, 'dffdf', '30-AUG-2016', '23.00', 23),
(19, 'dsdd', '17-AUG-2016', '12.33', 1231),
(4, 'ayala', '20/Aug/2016', '50.00', 3000),
(5, 'choora', '04-AUG-2016', '50.00', 50000),
(17, 'ghgh', '30-AUG-2016', '20.00', 346),
(16, 'ghfd', '30-AUG-2016', '56.00', 6556),
(15, 'aaad', '30-AUG-2016', '213.00', 1233),
(14, 'kera', '30-AUG-2016', '122.00', 2600),
(12, 'tuna', '30-AUG-2016', '32.50', 2500),
(13, 'ayala', '30-AUG-2016', '120.80', 10000),
(20, 'neymean', '30-AUG-2016', '34.89', 0),
(21, 'ghfd', '30-AUG-2016', '12.20', 120),
(22, 'kera', '31-AUG-2016', '100.00', 8500),
(23, 'choorra', '31-AUG-2016', '20.00', 1500),
(24, 'kera', '31-AUG-2016', '10.50', 1300),
(25, 'neymean', '31-AUG-2016', '20.70', 300),
(26, 'neymean', '01-SEP-2016', '20.40', 50000),
(27, 'machan', '01-SEP-2016', '15.50', 3500),
(28, 'choora', '01-SEP-2016', '15.30', 1200),
(29, 'neymean', '01-SEP-2016', '25.80', 20000),
(30, 'ayala', '13-OCT-2016', '200.00', 30),
(31, 'choora', '13-OCT-2016', '300.00', 100),
(32, 'kera', '13-OCT-2016', '500.00', 20);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `image` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `phone` varchar(12) NOT NULL,
  `id_type` varchar(15) DEFAULT NULL,
  `idcardno` varchar(30) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`staff_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `name`, `address`, `image`, `email`, `phone`, `id_type`, `idcardno`, `remark`) VALUES
(1, 'sam', 'kochi', 'fgd', 'fdgf', '4324653564', 'aadhar', 'dfgdfg543', ''),
(2, 'anu', 'aaaaaa', '', 'aaa', '984624326', 'fdsh', '54374ddgd', 'NULL'),
(3, 'anil', 'kali', '', 'jkffgf', '877856754', 'aadhar', 'ytf4345', 'NULL'),
(4, 'anup', 'sdhf', '', 'hgg', '6756474564', 'aadhar', 'hvkhg', 'NULL'),
(5, 'man', 'dsf', '', 'df', '874745677', 'aadhar', 'dff', 'NULL'),
(6, 'majeed', 'ernakulam,vazhakkala', '', 'Nill', '897564434', 'Election id', 'Aj104546', 'NULL'),
(7, 'arun', 'arunalayam', 'homeo_me.txt', 'arun92babu@gmail.com', '675765656', 'Election id', '21432bhbjbdsfsdsd', 'NULL'),
(8, 'anu', 'kochi', 'sadsa.jpg', 'anu@gmail.com', '87675665464', 'Election id', 'ed23425', 'NULL'),
(9, '123', 'arunalayam', 'mvd.jpg', 'aru n', '', 'Election id', 'ghad567', 'NULL'),
(10, '56757', 'ghg', 'main-slider.jpg', 'hgjgj', '', '-not selected-', 'ghjgj', 'NULL'),
(11, 'aju', 'ekm', '', 'fgfg@fhg.com', '', 'Election id', 'f4545', 'NULL');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(20) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  `stock_date` varchar(15) NOT NULL,
  PRIMARY KEY (`stock_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`stock_id`, `item`, `weight`, `stock_date`) VALUES
(3, 'ayla', '19.00', '08-AUG-2016'),
(4, 'choora', '50.00', '08-AUG-2016'),
(13, 'sdfsgfg', '56.40', '04-AUG-2016'),
(14, 'choora', '56.80', '31-AUG-2016'),
(15, 'choora', '3.20', '31-AUG-2016'),
(16, 'kera', '2.50', '31-AUG-2016'),
(17, 'neymean', '8.00', '31-AUG-2016'),
(18, 'choora', '21.50', '01-SEP-2016');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `trans_id` int(15) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NOT NULL,
  `sale_date` varchar(15) NOT NULL,
  `item` varchar(20) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  `price` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`trans_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`trans_id`, `cust_id`, `sale_date`, `item`, `weight`, `price`, `total`) VALUES
(1, 1, '0000-00-00', 'ayala', '34.00', 100, 0),
(6, 6, '03-AUG-2016', 'ayala', '2.50', 100, 250),
(39, 5, '13-OCT-2016', 'choora', '30.00', 20, 600),
(38, 5, '13-OCT-2016', 'ayala', '20.10', 10, 201),
(5, 3, '03-AUG-2016', 'ayala', '3.50', 20, 70),
(7, 5, '03-AUG-2016', 'arun', '21.00', 60, 1260),
(9, 3, '07-AUG-2016', 'choora', '50.00', 100, 5000),
(8, 5, '03-AUG-2016', 'ayala', '20.00', 400, 8000),
(10, 3, '07-AUG-2016', 'ayala', '10.00', 200, 2000),
(11, 3, '07-AUG-2016', 'kera', '10.00', 300, 3000),
(12, 3, '08-AUG-2016', 'ayala', '20.00', 200, 4000),
(13, 6, '10-AUG-2016', 'ayala', '3.50', 100, 350),
(14, 6, '10-AUG-2016', 'choora', '5.00', 115, 575),
(15, 4, '10-AUG-2016', 'choora', '5.00', 110, 550),
(16, 5, '11-AUG-2016', 'ayala', '5.20', 110, 572),
(17, 4, '10-AUG-2016', 'kera', '7.00', 120, 840),
(18, 3, '30-AUG-2016', 'ayala', '0.90', 24, 22),
(19, 3, '30-AUG-2016', 'ayala', '78.00', 110, 8580),
(20, 3, '30-AUG-2016', 'kera', '0.60', 12, 7),
(21, 3, '31-AUG-2016', 'ayala', '3.50', 100, 350),
(22, 3, '17-AUG-2016', 'choora', '2.40', 110, 264),
(23, 3, '31-AUG-2016', 'kera', '12.30', 110, 1353),
(24, 4, '31-AUG-2016', 'meen', '3.50', 50, 175),
(25, 4, '31-AUG-2016', 'ney', '4.00', 400, 1600),
(26, 6, '31-AUG-2016', 'choora', '10.10', 110, 1111),
(27, 6, '31-AUG-2016', 'ayala', '5.00', 100, 500),
(28, 6, '31-AUG-2016', 'kera', '8.50', 120, 1020),
(29, 6, '31-AUG-2016', 'neymean', '5.00', 400, 2000),
(30, 3, '01-SEP-2016', 'ayala', '5.50', 100, 550),
(31, 3, '01-SEP-2016', 'choora', '5.90', 110, 649),
(32, 3, '01-SEP-2016', 'kera', '7.60', 115, 874),
(33, 3, '01-SEP-2016', 'neymean', '3.60', 400, 1440),
(34, 4, '01-SEP-2016', 'thilopiya', '45.00', 120, 5400),
(35, 4, '29-SEP-2016', 'ayala', '200.00', 12, 2400),
(36, 4, '29-SEP-2016', 'choora', '300.00', 20, 6000),
(37, 4, '29-SEP-2016', 'ney', '10.20', 10, 102);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
