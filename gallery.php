<!DOCTYPE html>
<html>
<!-- Mirrored from webdesign-finder.com/phototravel/gallery_item.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 10 May 2016 09:31:46 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Photo Travel">
    <meta name="keywords" content="Photo Travel">
    <title>Photo Travel</title>
    <!--pageMeta-->
    <!-- Loading Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Loading Elements Styles -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/rt_icons.css" rel="stylesheet">
    <link href="css/prettyPhoto.css"  rel="stylesheet"/>
    <link href="css/flexslider.css"  rel="stylesheet"/>
    <!-- Favicons -->
    <link rel="icon" href="images/favicons/favicon.png">
    <link rel="apple-touch-icon" href="images/favicons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-touch-icon-114x114.png">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="scripts/html5shiv.js"></script>
    <script src="scripts/respond.min.js"></script>
    <![endif]-->
    <!--headerIncludes-->
    </head>
    <body>
    <div id="preloader">
        <div class="loading-data">
            <div class="dot"></div>
            <div class="dot2"></div>
        </div>
    </div>
     <div id="wrap">
         <nav class="navbar">
             <div class="container">
                 <a href="#" class="navbar-brand"><span>Photo</span><img height="29" alt="Your logo" src="images/logo.png">Travel</a>
                 <button class="navbar-toggle menu-collapse-btn collapsed" data-toggle="collapse" data-target=".navMenuCollapse"  aria-hidden="true">
                     <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                 <div class="social_search-section">
                     <form id="search_form" class="line-form" novalidate>
                         <div class="input-group search-header">
                             <input class="form-control search-input" type="text" name="search" placeholder="Search">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary search-btn" data-loading-text="•••"  aria-hidden="true"><span class="icon center-icon icon-search5"></span></button>
                            </span>
                         </div>
                     </form>
                     <ul class="soc-list">
                         <li><a href="#" target="_blank" title="facebook"><span class="icon soc_facebook">f</span></a></li>
                         <li><a href="#" target="_blank" title="twitter"><span class="icon soc_twitter">l</span></a></li>
                         <li><a href="#" target="_blank" title="google plus"><span class="icon soc_google">g</span></a></li>
                     </ul>
                 </div>
                 <div class="collapse navbar-collapse navMenuCollapse">
                     <ul class="nav">
                         <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Homepages</a>
                             <ul class="dropdown-menu">
                                 <li><a href="homepage_1.html">Homepage 1</a>
                                 <li><a href="homepage_2.html">Homepage 2</a>
                                 <li><a href="homepage_dark.html">Homepage dark</a>
                                 <li><a href="homepage_boxed.html">Homepage boxed</a>
                             </ul>
                         </li>
                         <li class="menu-item dropdown active"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Gallery</a>
                             <ul class="dropdown-menu">
                                 <li><a href="gallery_item.html">gallery item</a>
                                 <li><a href="gallery_regular.html">gallery regular</a>
                                 <li><a href="gallery_extended.html">gallery extended</a>
                                 <li><a href="gallery_fullwidth.html">gallery fullwidth</a>
                             </ul>
                         </li>
                         <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog</a>
                             <ul class="dropdown-menu">
                                 <li><a href="blog_mosaic.html">blog mosaic</a>
                                 <li><a href="blog_post_image.html">blog post image</a>
                                 <li><a href="blog_post_video.html">blog post video</a>
                                 <li><a href="blog_right_sidebar.html">blog right sidebar</a>
                             </ul>
                         </li>
                         <li><a href="about.html">about</a></li>
                         <li><a href="contacts.html">Contacts</a></li>
                     </ul>
                 </div>
             </div>
         </nav>
		<header id="breadcrumbs" class="dark">
            <div class="container">
                <div class="row">
                    <ul>
                        <li><a href="#" title="Homepage">Homepage</a> </li>
                        <li><a href="#" title="Homepage">Pages</a> </li>
                        <li><a href="#" title="Homepage" class="active">Item</a> </li>
                    </ul>
                </div>
            </div>
		</header>

        <section class="slider light">
            <div class="container-triplex">
                <div id="slider" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="images/latest_photo_11.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>75</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>31</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                November 22, 2014 <br>
                                                October 19, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.588408110381!2d-0.15834819967910493!3d51.52076673608002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ace9a2e67d7%3A0xd458de8d0fdc498e!2zQmFrZXIgU3QsIE1hcnlsZWJvbmUsIExvbmRvbiwg0JLQtdC70LjQutC-0LHRgNC40YLQsNC90LjRjw!5e0!3m2!1sru!2sua!4v1457952134423"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_22.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>83</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>72</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                December 22, 2012 <br>
                                                November 11, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6402236.224204002!2d-121.51361300184796!3d38.41624087926714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80990aa1f8deb471%3A0xcf47038aaafc95b3!2z0J3QtdCy0LDQtNCwLCDQodCo0JA!5e0!3m2!1sru!2sua!4v1459385906165"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_55.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>32</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>24</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                November 05, 2013 <br>
                                                October 19, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.588408110381!2d-0.15834819967910493!3d51.52076673608002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ace9a2e67d7%3A0xd458de8d0fdc498e!2zQmFrZXIgU3QsIE1hcnlsZWJvbmUsIExvbmRvbiwg0JLQtdC70LjQutC-0LHRgNC40YLQsNC90LjRjw!5e0!3m2!1sru!2sua!4v1457952134423"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_33.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>83</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>72</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                December 22, 2012 <br>
                                                November 11, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6402236.224204002!2d-121.51361300184796!3d38.41624087926714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80990aa1f8deb471%3A0xcf47038aaafc95b3!2z0J3QtdCy0LDQtNCwLCDQodCo0JA!5e0!3m2!1sru!2sua!4v1459385906165"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_44.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>75</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>31</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                November 22, 2014 <br>
                                                October 19, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.588408110381!2d-0.15834819967910493!3d51.52076673608002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ace9a2e67d7%3A0xd458de8d0fdc498e!2zQmFrZXIgU3QsIE1hcnlsZWJvbmUsIExvbmRvbiwg0JLQtdC70LjQutC-0LHRgNC40YLQsNC90LjRjw!5e0!3m2!1sru!2sua!4v1457952134423"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_67.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>83</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>72</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                December 22, 2012 <br>
                                                November 11, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6402236.224204002!2d-121.51361300184796!3d38.41624087926714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80990aa1f8deb471%3A0xcf47038aaafc95b3!2z0J3QtdCy0LDQtNCwLCDQodCo0JA!5e0!3m2!1sru!2sua!4v1459385906165"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_55.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>75</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>31</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                November 22, 2014 <br>
                                                October 19, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.588408110381!2d-0.15834819967910493!3d51.52076673608002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ace9a2e67d7%3A0xd458de8d0fdc498e!2zQmFrZXIgU3QsIE1hcnlsZWJvbmUsIExvbmRvbiwg0JLQtdC70LjQutC-0LHRgNC40YLQsNC90LjRjw!5e0!3m2!1sru!2sua!4v1457952134423"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <img src="images/latest_photo_44.jpg" alt="" />
                            <a class="info-slider"></a>
                            <div class="info-open">
                                <div class="info-row">
                                    <div class="info-desc">
                                        <h4>Description</h4>
                                        Stryn, Norway <br>
                                        <a href="#" title="bridge">#bridge</a>
                                        <a href="#" title="fog">#fog</a>
                                        <a href="#" title="misty">#misty</a>
                                        <a href="#" title="bay">#bay</a>
                                    </div>
                                    <div class="info-details">
                                        <h4>Details</h4>
                                        <div class="fl detail-views">
                                            Views:<br>
                                            <span>83</span>
                                        </div>
                                        <div class="fl detail-views">
                                            Likes:<br>
                                            <span>72</span>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="details_device">
                                            <p>
                                                Canon EOS 6D <br>
                                                EF17-40mm f/4L USM <br>
                                                26mm / ƒ/9.0 / 1/400s / ISO 400
                                            </p>
                                        </div>
                                        <div class="photo-create">
                                            <div class="get-create">
                                                taken<br>
                                                uploaded
                                            </div>
                                            <div class="date-create">
                                                December 22, 2012 <br>
                                                November 11, 2015
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-locator">
                                        <h4>Location</h4>
                                        <div class="embed-responsive embed-responsive-slider">
                                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6402236.224204002!2d-121.51361300184796!3d38.41624087926714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80990aa1f8deb471%3A0xcf47038aaafc95b3!2z0J3QtdCy0LDQtNCwLCDQodCo0JA!5e0!3m2!1sru!2sua!4v1459385906165"></iframe>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </li>
                    </ul>
                     </div>
                <div class="author_carousel_actions">
                    <div class="slider-author">
                                <a href="#" title="James Anderson" class="ava-slider">
                                    <img src="images/ava-4-comment.jpg" alt="James Anderson">
                                </a>
                                <div class="slider_title_author">
                                    <h2>Misty Bridge Going Nowhere</h2>
                                    <a href="#" class="slider-author-name">by James Anderson</a>
                                </div>
                            </div>
                    
                    <div class="carousel-strap">
                        <div id="carousel" class="flexslider">
                            <ul class="slides">
                                <li>
                                    <img src="images/thumb1.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb2.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb3.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb4.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb5.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb6.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb3.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="images/thumb5.jpg" alt="" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>

        </section>
         <section class="gellery_item light">
             
         </section>
         <div class="cross-socail_slider light">
             <ul class="soc-list">
                 <li><a href="#" target="_blank" title="facebook"><span class="icon soc_facebook">f</span></a></li>
                 <li><a href="#" target="_blank" title="twitter"><span class="icon soc_twitter">l</span></a></li>
                 <li><a href="#" target="_blank" title="google plus"><span class="icon soc_google">g</span></a></li>
                 <li><a href="#" target="_blank" title="linkedin"><span class="icon soc_linkedin">i</span></a></li>
                 <li><a href="#" target="_blank" title="flickr"><span class="icon soc_pinterest">:</span></a></li>
                 <li><a href="#" target="_blank" title="pinterest"><span class="icon soc_flickr">n</span></a></li>
                 <li><a href="#" target="_blank" title="rss"><span class="icon soc_rss">r</span></a></li>
             </ul>
         </div>
         <section id="instagram">
             <h4>Follow Instagram</h4>
             <ul class="instagram-list gallery">
                 <li> <a href="images/insta_01.jpg" class="PhotoZoom" title="Description" ><img src="images/insta_01.jpg" alt=""></a></li>
                 <li> <a href="images/insta_02.jpg" class="PhotoZoom" title="Description"><img src="images/insta_02.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_03.jpg" class="PhotoZoom" title="Description"><img src="images/insta_03.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_04.jpg" class="PhotoZoom" title="Description"><img src="images/insta_04.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_05.jpg" class="PhotoZoom" title="Description"><img src="images/insta_05.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_06.jpg" class="PhotoZoom" title="Description"><img src="images/insta_06.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_07.jpg" class="PhotoZoom" title="Description"><img src="images/insta_07.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_08.jpg" class="PhotoZoom" title="Description"><img src="images/insta_08.jpg" alt=""></a> </li>
             </ul>
         </section>
        <footer class="footer-social-sec dark">
            <div class="container">
                <div class="row">
                    <ul class="social-footer text-center soc-inv">
                        <li><a href="#" title="facebook"><span class="icon soc_facebook">f</span>Facebook</a></li>
                        <li><a href="#" title="twitter"><span class="icon soc_twitter">l</span>Twitter</a></li>
                        <li><a href="#" title="google"><span class="icon soc_google">g</span>Google Plus</a></li>
                        <li><a href="#" title="linkedin"><span class="icon soc_linkedin">i</span>Linked In</a></li>
                        <li><a href="#" title="pinterest"><span class="icon soc_pinterest">:</span>Pinterest</a></li>
                        <li><a href="#" title="flickr"><span class="icon soc_flickr">n</span>Flickr</a></li>
                        <li><a href="#" title="rss"><span class="icon soc_rss">r</span>RSS</a></li>
                    </ul>
                </div>
            </div>
        </footer>
        <footer class="underground dark">
            <div class="container">
                <div class="row">
                    <p>
                        &copy; PhotoTravel 2016 | Created with <span class="icon-heart"></span> by <span> MW Templates </span>
                    </p>
                </div>
            </div>
        </footer>
     </div>
    <!-- /#wrap -->
            <script src="scripts/jquery-1.12.3.min.js"></script>
            <script src="scripts/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
            <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
            <script src="scripts/jquery.flexslider-min.js"></script>
            <script src="scripts/preloader.js"></script>
</body>
<!-- Mirrored from webdesign-finder.com/phototravel/gallery_item.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 10 May 2016 09:31:46 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
</html>