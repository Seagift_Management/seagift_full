<?php
include("connect.php"); 
session_start();
$sid=mysql_real_escape_string($_GET['did']);
//error_reporting(0);
if($_SESSION['din']==null)
    {
		header("Location:index.php");
	}
	
//echo("Welcome"." ".$_SESSION['din']);
 
  
  if(isset($_POST['view']))
				{
				$doe=$_POST['doe'];
				$_SESSION['dat']=$doe;
				
				}

	
?>	

<!--shows expense details  -->
<!DOCTYPE html>
<html>
<!-- Mirrored from webdesign-finder.com/phototravel/contacts.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 10 May 2016 09:32:01 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Photo Travel">
    <meta name="keywords" content="Photo Travel">
    <title>Seagift</title>
    <!--pageMeta-->
    <!-- Loading Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Loading Elements Styles -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/rt_icons.css" rel="stylesheet">
    <link href="css/prettyPhoto.css"  rel="stylesheet"/>
    <!-- Favicons -->
    <link rel="icon" href="images/favicons/favicon.png">
    <link rel="apple-touch-icon" href="images/favicons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-touch-icon-114x114.png">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="scripts/html5shiv.js"></script>
    <script src="scripts/respond.min.js"></script>
    <![endif]-->
    <!--headerIncludes-->
    
<!--date picker -->
<link rel="stylesheet" type="text/css" media="all" href="css/jsDatePick_ltr.min.css" />
<!-- 
	OR if you want to use the calendar in a right-to-left website
	just use the other CSS file instead and don't forget to switch g_jsDatePickDirectionality variable to "rtl"!
	
	<link rel="stylesheet" type="text/css" media="all" href="jsDatePick_ltr.css" />
-->
<script type="text/javascript" src="js/jquery.1.4.2.js"></script>
<script type="text/javascript" src="js/jsDatePick.jquery.min.1.3.js"></script>
<script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"inputField",
			dateFormat:"%d-%M-%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
	};
	</script>

<!--end of date picker -->
    
    
    
    </head>
    <body>
    <div id="preloader">
        <div class="loading-data">
            <div class="dot"></div>
            <div class="dot2"></div>
        </div>
    </div>
     <div id="wrap">
        <nav class="navbar">
             <div class="container">
                 <a href="#" class="navbar-brand"><span>SEA</span><img height="29" alt="Your logo" src="images/logo.png">GIFT</a>
                 <button class="navbar-toggle menu-collapse-btn collapsed" data-toggle="collapse" data-target=".navMenuCollapse"  aria-hidden="true">
                     <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                 <div class="social_search-section">
                     
                     <ul class="soc-list">
                        <li><a href="lgout.php" title="logout"><span class="icon soc_facebook">L</span></a></li>
                     </ul>
                 </div>
                 <div class="collapse navbar-collapse navMenuCollapse">
                    <ul class="nav">
                       <li><a href="staffhome.php">HOME</a></li>
                           
                        </li>
                        
                        <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">VIEW</a>
                            <ul class="dropdown-menu">
                             <li><a href="vcust.php">CUSTOMER</a>
                             <li><a href="vexp.php">EXPENSE</a>
                             <li><a href="vincome.php">INCOME</a>
							 <li><a href="vpur.php">PURCHASE</a>
							 <li><a href="vcat.php">CATEGORY</a>
                             <li><a href="vstock.php">STOCK</a> 
                                
                            </ul>
                        </li>
                         <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">ENTRY</a>
                         <ul class="dropdown-menu">
                             <li><a href="pur_entry.php">PURCHASE ENTRY</a>
                             <li><a href="trans.php">TRANSACTION ENTRY</a>
                             <li><a href="income.php">INCOME ENTRY</a>
                             <li><a href="expense.php">EXPENSE ENTRY</a>
                             <li><a href="stock.php">STOCK ENTRY</a>
                             <li><a href="return.php">RETURNED GOODS</a>
                             
                                
                            </ul></li>
                          <li><a href="staffbill.php">BILLING</a></li>
                           <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">VIEW BILLS</a>
                         <ul class="dropdown-menu">
                             <li><a href="staff_vbill.php">CUSTOMER</a>
                             <li><a href="pdf.php">CASH BALANCE</a>
                             <li><a href="income_vbill.php">INCOME</a>
                             <li><a href="expense_vbill.php">EXPENSE</a>
                             <li><a href="purchase_vbill.php">PURCHASE</a>
                             <li><a href="stock_vbill.php">STOCK</a> 
                                
                            </ul></li>
                         
                    </ul>
                 </div>
             </div>
         </nav>
		<header id="breadcrumbs" class="dark">
            <div class="container">
                <div class="row">
                    <ul>
                        <li><a href="#" title="Homepage">HOME</a> </li>
                        
                        <li><a href="#" title="Homepage" class="active">BILLING</a> </li>
                    </ul>
                </div>
            </div>
		</header>

        <main id="contacts" class="text-center">
           
            
            <div class="row common-block light">
                 <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                      
                      <h4>BILL GENERATION</h4>
                         <div class="form-container">
                                 <form role="form" name="staffbill" action="staffbill.php" method="post" >
<script>								 
function  chkDOE()//--------Validation for Date field--------
{
  		var doe=staffbill.doe.value;
				
               if(doe =='')		
			  		{
   			  		    document.getElementById("dte").innerHTML="Please Pick Date!";
						//alert("Please Enter Date of Purchase!");
				  		document.staffbill.doe.focus();
			  			return false;
			  		}
					if(doe.length<11)
				  	{
				  		document.getElementById("dte").innerHTML="Please Pick proper Date.!";
						
				  		document.staffbill.doe.focus();
			  			return false;
				  	} 
				 else
	              {
		           document.getElementById("dte").innerHTML="";
	              }
}  
</script>								 
 
      <div class="panel-body">


 
  <div class="form-group">
    <table class='table' border='1' size='50'>
        
                         <thead>
 <tr class='info'><div class="form-group">
    <label for="exampleInputEmail1">Date<span class="mand">*</span></label>
    <span id="sprytextfield1">
     <input class="form-group" type="text" name="doe" size="12" id="inputField" placeholder="Pick date" required onBlur="chkDOE()"><span class="mandedit" id="dte"></span>
   <button type="submit" name="view" class="btn btn-sm btn-warning"  > View</button>
    </div>
    </form>
    
   <form name="myForm" action="bilgen.php"  method="post" target="_blank" name="staffbill"  onsubmit="return userValidation()">
   
     <script >    
	 function chkCUST()//--------Validation for ID Number field--------
{	
  var customer=myForm.select.value;
				
if(customer =='--select--')		//--------Validation for ID Type field--------
		  		    {
			  		 document.getElementById("cust").innerHTML="Please select customer...!";
					 	
			  		 document.myForm.select.focus();
		  			 return false	
					}	
                  else
	              {
		           document.getElementById("cust").innerHTML="";
	              }
}	
 
    
	
	</script>
     <div class="form-group">
      <label for="exampleInputEmail1">Customer  name<span class="mand">*</span></label>
                                            
                                            <select class="form-control" name="select" id="select" placeholder="name" onBlur="chkCUST()">
                                            <option>--select--</option>
<script>
function myFunction() {
    alert("Are you sure! want to Generate Bill");
}
</script>                                        
                                               <?php
if(isset($_POST['view']))

{
	$doe=$_POST['doe'];
	   
     
   
	
   $ne=mysql_query("SELECT DISTINCT  `cust_id`,`sale_date` FROM  `transaction` WHERE  `sale_date` LIKE  '".$doe."'");
	while($did=mysql_fetch_array($ne))
		{
   $name=$did[0];
          
             
            
            ?>
                                              
                                                <option> <?php
				 $n=mysql_query("SELECT * FROM customer where cust_id='".$name."'");
	while($d=mysql_fetch_array($n))
		{
				
				  echo $d[1]; }?></option>
<?php
}
?>
                                                
                                            </select>
											<span class="mandedit" id="cust"></span><br>
                                            <button type="submit" name="gen" class="btn btn-sm btn-warning"  onclick="myFunction()"> Generate Bill</button>
                                        </div>
    

    
    </tr>
                        
 <tr class='info'>
  	
    <th>Customer Name</th>
    
    <th>Status</th>
    
       </tr></thead>
       
  <tbody>
  
  <!--shows all bill  details -->
 <?php
	
   $ne=mysql_query("SELECT DISTINCT  `cust_id`,`sale_date` FROM  `transaction` WHERE  `sale_date` LIKE  '".$doe."'");
	while($did=mysql_fetch_array($ne))
		{
   $name=$did[0];
          
             
            
            ?>
           <tr class='danger'>
                <td> 
                <?php
				 $n=mysql_query("SELECT * FROM customer where cust_id='".$name."'");
	while($d=mysql_fetch_array($n))
		{
				
				  echo $d[1]; }?></td>
               
                <td>
                 <?php
				 $p=mysql_query("SELECT * FROM bill where cust_id='".$name."' and bill_date='".$doe."'");
	if($q=mysql_fetch_array($p))
		{
      
				 echo Generated;
				 }
				 else
				 {
				 echo ("Not Generated");
				 }
				 ?>
                 </td>
               
               
          
                <?php
			
		}
		}
			?>
                
                
               
                 
                
            
  </tbody>
</table>



</div>
    
  
    
 
 
</form>              </div>
                 </div>
            </div>

        </main>
         <section id="instagram">
             <h4>Gallery</h4>
             <ul class="instagram-list gallery">
                 <li> <a href="images/insta_01.jpg" class="PhotoZoom" title="image" ><img src="images/insta_01.jpg" alt=""></a></li>
                 <li> <a href="images/insta_02.jpg" class="PhotoZoom" title="image"><img src="images/insta_02.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_03.jpg" class="PhotoZoom" title="image"><img src="images/insta_03.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_04.jpg" class="PhotoZoom" title="image"><img src="images/insta_04.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_05.jpg" class="PhotoZoom" title="image"><img src="images/insta_05.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_06.jpg" class="PhotoZoom" title="image"><img src="images/insta_06.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_07.jpg" class="PhotoZoom" title="image"><img src="images/insta_07.jpg" alt=""></a> </li>
                 <li> <a href="images/insta_08.jpg" class="PhotoZoom" title="image"><img src="images/insta_08.jpg" alt=""></a> </li>
             </ul>
         </section>
        <footer class="footer-social-sec dark">
            <div class="container">
               
            </div>
        </footer>
        <footer class="underground dark">
            <div class="container">
                <div class="row">
                    <p>
                        &copy; Seagift 2016 | Created  by <span> Arun.s.babu and shafeer </span>
                    </p>
                </div>
            </div>
        </footer>
     </div>
    <!-- /#wrap -->
     <script src="scripts/jquery.validate.min.js"></script>
     <script src="scripts/placeholders.jquery.min.js"></script>
     <script>
         function runScript() {
             $('#contact_form').validate({
                 onfocusout: false,
                 onkeyup: false,
                 rules: {
                     name: "required",
                     message: "required",
                     phone: "required"
                 },
                 errorPlacement: function (error, element) {
                     error.insertAfter(element);
                 },
                 messages: {
                     name: "What's your name?",
                     message: "Type your message",
                     phone: "Enter your phone"
                 },

                 highlight: function (element) {
                     $(element)
                             .text('').addClass('error')
                 },

                 success: function (element) {
                     element
                             .text('').addClass('valid')
                 }
             });

             $('#contact_form').submit(function () {
                 // submit the form
                 if ($(this).valid()) {
                     $('#contact_submit').button('loading');
                     var action = $(this).attr('action');
                     $.ajax({
                         url: action,
                         type: 'POST',
                         data: {
                             contactname: $('#contact_name').val(),
                             contactphone: $('#number').val(),
                             contactmessage: $('#contact_message').val()
                         },
                         success: function () {
                             $('#contact_submit').button('reset');
                             //Use modal popups to display messages
                             $('#modalContactSuccess').modal('show');
                         },
                         error: function () {
                             $('#contact_submit').button('reset');
                             //Use modal popups to display messages
                             $('#modalContactError').modal('show');
                         }
                     });
                 } else {
                     $('#contact_submit').button('reset')
                 }
                 return false;
             });
         }

         runScript();

         document.addEventListener('reload-script',function(){
             runScript();
         });
     </script>
     <script src="scripts/jquery-1.12.3.min.js"></script>
     <script src="scripts/jquery.prettyPhoto.js"></script>
     <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
     <script src="scripts/preloader.js"></script>
     <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
     <script>
         var map;
         var egglabs = new google.maps.LatLng(51.9730757, 1.1502547);
         var mapCoordinates = new google.maps.LatLng(51.9730757, 1.1502547);
         var markers = [];
         var image = new google.maps.MarkerImage(
                 'images/marker.png',
                 new google.maps.Size(84,56),
                 new google.maps.Point(0,0),
                 new google.maps.Point(42,56)
         );

         function addMarker()
         {
             markers.push(new google.maps.Marker({
                 position: egglabs,
                 raiseOnDrag: false,
                 icon: image,
                 map: map,
                 draggable: false
             }));

         }

         function initialize() {
             var mapOptions = {
                 backgroundColor: "#f2e5d4",
                 zoom: 14,
                 disableDefaultUI: true,
                 center: mapCoordinates,
                 mapTypeId: google.maps.MapTypeId.ROADMAP,
                 styles: [
                     {
                         "featureType": "landscape.natural",
                         "elementType": "geometry.fill",
                         "stylers": [
                             { "color": "#f2e5d4" }
                         ]
                     },
                     {
                         "featureType": "landscape.man_made",
                         "stylers": [
                             { "color": "#f2e5d4" },
                             { "visibility": "off" }
                         ]
                     },
                     {
                         "featureType": "water",
                         "stylers": [
                             { "color": "#80C8E5" },
                             { "saturation": 0 }
                         ]
                     },
                     {
                         "featureType": "road.arterial",
                         "elementType": "geometry",
                         "stylers": [
                             { "color": "#e5d8c8" }
                         ]
                     }
                     ,{
                         "elementType": "labels.text.stroke",
                         "stylers": [
                             { "visibility": "off" }
                         ]
                     }
                     ,{
                         "elementType": "labels.text",
                         "stylers": [
                             { "color": "#3e3933" }
                         ]
                     }

                     ,{
                         "featureType": "road.local",
                         "stylers": [
                             { "color": "#dedede" }
                         ]
                     }
                     ,{
                         "featureType": "road.local",
                         "elementType": "labels.text",
                         "stylers": [
                             { "color": "#989898" }
                         ]
                     }
                     ,{
                         "featureType": "transit.station.bus",
                         "stylers": [
                             { "saturation": -57 }
                         ]
                     }
                     ,{
                         "featureType": "road.highway",
                         "elementType": "labels.icon",
                         "stylers": [
                             { "visibility": "off" }
                         ]
                     },{
                         "featureType": "poi",
                         "stylers": [
                             { "visibility": "off" }
                         ]
                     }

                 ]

             };
             map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
             addMarker();

         }
         google.maps.event.addDomListener(window, 'load', initialize);

     </script>
</body>
<!-- Mirrored from webdesign-finder.com/phototravel/contacts.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 10 May 2016 09:32:02 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
</html>